typedef char*ztr;


ztr lambda(){
	ztr const lambda= "Cadena vacia (Lambda)";
	return lambda;
}

ztr empty(){
	//ztr nullztr=(ztr)malloc(501);
	ztr nullztr="";
	return nullztr;
	//return NULL;
	//return ;
}

int contains(char c,ztr ztring){
	int i=0;
	while(ztring[i]){
		if(c==ztring[i]){
			return 1;break;
		}
		i++;
	}
	return 0;
}

int ztrlen(ztr ztring){
	int i=0;
	//if(isempty(ztring))
	//	return i;
	while(ztring[i])
		i++;
	return i;
}

int isempty(ztr ztring){
	return (ztrlen(ztring)>0)? 0: 1;
	//return ztring==NULL;
	//return ztring=='\0';
}

void getztring(ztr ztring){
	//scanf(" %[^\n]",ztring);
	//ztring=(ztr)malloc(501);
	//ztring=empty();
	scanf(" %[^\n]",ztring);
	if(isempty(ztring))
		ztring=lambda();
}

ztr getztring2(){
	ztr gztr=(ztr)malloc(501);
	scanf(" %[^\n]",gztr);
	return gztr;
}

int max(int a,int b){
	return a<b?b:a;
}

int ztrcmp(ztr cad1,ztr cad2){
	int i=0;
	int cntr=max(ztrlen(cad1),ztrlen(cad2));
	while(i<cntr){
		if(cad1[i]!=cad2[i])
			return 1;
		i++;
	}
	return 0;
}

ztr concat(ztr cad1,ztr cad2){
	if(isempty(cad1)&&isempty(cad2))
		return lambda();
	if(isempty(cad2))
		return cad1;
	
	else if(isempty(cad1))
		return cad2;
	
	else{
		int i,k;
	ztr conc=(ztr)malloc(ztrlen(cad1)+ztrlen(cad2)+1);
	i=0;k=0;
	
	while(cad1[i]){
		conc[i]=cad1[i];
		i++;
	}
	
	while(cad2[k]){
		conc[i]=cad2[k];
		k++;
		i++;
	}
	
	conc[i]='\0';
	return conc;
	}
}

ztr prefix(ztr ztring, int num){
	if(num==0)
		return lambda();
	int tam;
	tam=ztrlen(ztring);
	if(num<tam){
		ztr pre=(ztr)malloc(num+1);
		int i=0;
		while(num){
			pre[i]=ztring[i];
			num--;
			i++;
		}
		pre[i]='\0';
		return pre;
	}
	else if(num>tam)
		return "El valor introducido rebasa la longitud de la cadena";
	else
		return ztring;	
}

ztr prefix_rev(ztr ztring,int num){
	int l=ztrlen(ztring);
	int dif=l-num;
	if(dif<0)
		return prefix(ztring,l+1);
	else if(dif>0)
		prefix(ztring,dif);
	else
		return prefix(ztring,0);
}

ztr sufix_rev(ztr ztring,int num){
	int tam=ztrlen(ztring);
	if(num<tam)
		return ztring+num;
	else if(num>tam)
		return "El valor introducido rebasa la longitud de la cadena";
	else 
		return lambda();
}

ztr sufix(ztr ztring,int num){
	int l=ztrlen(ztring);
	int dif=l-num;
	if(dif<0)
		return sufix_rev(ztring,l+1);
	else if(dif>0)
		sufix_rev(ztring,dif);
	else
		return sufix_rev(ztring,0);
}

ztr subztring(ztr ztring,int pref,int suf){
	return prefix_rev(sufix_rev(ztring,pref),suf);
}

ztr reverse(ztr ztring){
	if(isempty(ztring))
		return lambda();
	
	int l,i=0;
	l=ztrlen(ztring);
	ztr rev=(ztr)malloc(l);
	while(l){
		rev[i]=ztring[l-1];
		i++;
		l--;
	}
	rev[i]='\0';
	return rev;
}

ztr power(ztr ztring, int pow){
	if(isempty(ztring))
		return lambda();
	
	ztr spow=(ztr)malloc(ztrlen(ztring)*pow+1);
	spow=empty();
	if(pow>0){
		while(pow){
			spow=concat(spow,ztring);
			pow--;
		}
	}
	else if(pow<0){
		while(-pow){
			spow=concat(spow,reverse(ztring));
			pow++;
		}
	}
	else
		spow=lambda();
	return spow;
}

int indexof(char c,ztr ztring){
	int i=0;
	while(ztring[i]){
		if(c==ztring[i]){
			return i+1;break;
		}
		i++;
	}
	return -1;
}

ztr erase_c(char c,ztr ztring){
	int pstn1=indexof(c,ztring);
	if(pstn1<0){
		return ztring;
	}
	
	int pstn2=ztrlen(ztring)-pstn1;
	ztr part1=(ztr)malloc(pstn1);
	ztr part2=(ztr)malloc(pstn2+1);
	if(pstn1==1)
		part1=empty();
	else
		part1=prefix(ztring,pstn1-1);
		
	if(pstn2==0)
		part2=empty();
	else
		part2=sufix(ztring,pstn2);
		
	return concat(part1,part2);
}

void show_ztrs(ztr cad1,ztr cad2){
	if(isempty(cad1))
		cad1=lambda();
	if(isempty(cad2))
		cad2=lambda();
	printf("\n Las cadenas introducidas son:\n\n  Cadena u= %s\n\n  Cadena v= %s\n\n",cad1,cad2);
}



void menu_main(ztr cad1,ztr cad2,int opt);


void menu_concat(ztr cad1,ztr cad2){
	system("cls");
	puts("========\t2.- CONCATENAR CADENAS\t========\n\n\n");
	show_ztrs(cad1,cad2);
	printf("\n  Cadena uv= %s\n\n  Cadena vu= %s\n",concat(cad1,cad2),concat(cad2,cad1));
	printf("\nPresiona %cc%c para cambiar las cadenas, cualquier otra tecla para volver al men%c",34,34,163);
	if(getch()=='c')
		return menu_main(cad1,cad2,1);
	else
		return menu_main(cad1,cad2,0);	
}

void sw_prefix(short sw){
	(sw==1)?printf("\n Introduce el tama%co del prefijo: ",164):
		printf("\n Introduce el n%cmero de caracteres a eliminar (de derecha a izquierda): ",163);		
}

void sw_sufix(short sw){
	(sw==1)?printf("\n Introduce el tama%co del sufijo: ",164):
		printf("\n Introduce el n%cmero de caracteres a eliminar (de izquierda a derecha): ",163);		
}

void menu_prefix(ztr cad1,ztr cad2,int opt){
	system("cls");
	puts("========\t3.- OBTENER PREFIJO DE LAS CADENAS\t========\n\n\n");
	show_ztrs(cad1,cad2);
	int tam=0,verify=0;
	printf(" \n\nEscoge una opci%cn:\n\n 1.-Introducir la longitud del prefijo\n\n 2.-Introducir n%cmero de caracteres a eliminar (de derecha a izquierda)\n\n ",162,163);
	while(opt==0){
		tam=scanf("%d",&opt);
		getchar();
		if((opt!=1)&&(opt!=2)||(tam!=1))
			return menu_prefix(cad1,cad2,0);
		else
			break;
	}
	
	tam=0;
	
	switch(opt){
		case 1:
			while(verify==0){
				sw_prefix(1);
				verify=scanf("%d",&tam);
				getchar();
				if(verify==1)
					break;
				else
					return menu_prefix(cad1,cad2,1);
			}
			
			printf("\n  Prefijo de la cadena u= %s\n\n  Prefijo de la cadena v= %s\n",prefix(cad1,tam),prefix(cad2,tam));
			break;
		
		case 2:
			while(verify==0){
				sw_prefix(2);
				verify=scanf("%d",&tam);
				getchar();
				if(verify==1)
					break;
				else
					return menu_prefix(cad1,cad2,2);
			}
			
			printf("\n  Prefijo de la cadena u= %s\n\n  Prefijo de la cadena v= %s\n",prefix_rev(cad1,tam),prefix_rev(cad2,tam));
			break;
	}
	printf("\nPresiona %cc%c para cambiar las cadenas, cualquier otra tecla para volver al men%c",34,34,163);
	if(getch()=='c')
		return menu_main(cad1,cad2,1);
	else
		return menu_main(cad1,cad2,0);
}

void menu_sufix(ztr cad1,ztr cad2,int opt){
	system("cls");
	puts("========\t4.- OBTENER SUFIJO DE LAS CADENAS\t========\n\n\n");
	show_ztrs(cad1,cad2);
	int tam=0,verify=0;
	printf(" \n\nEscoge una opci%cn:\n\n 1.-Introducir la longitud del sufijo\n\n 2.-Introducir n%cmero de caracteres a eliminar (de izquierda a derecha)\n\n ",162,163);
	while(opt==0){
		tam=scanf("%d",&opt);
		getchar();
		if((opt!=1)&&(opt!=2)||(tam!=1))
			return menu_sufix(cad1,cad2,0);
		else
			break;
	}
	
	tam=0;
	
	switch(opt){
		case 1:
			while(verify==0){
				sw_sufix(1);
				verify=scanf("%d",&tam);
				getchar();
				if(verify==1)
					break;
				else
					return menu_sufix(cad1,cad2,1);
			}
			
			printf("\n  Sufijo de la cadena u= %s\n\n  Sufijo de la cadena v= %s\n",sufix(cad1,tam),sufix(cad2,tam));
			break;
		
		case 2:
			while(verify==0){
				sw_sufix(2);
				verify=scanf("%d",&tam);
				getchar();
				if(verify==1)
					break;
				else
					return menu_sufix(cad1,cad2,2);
			}
			
			printf("\n  Sufijo de la cadena u= %s\n\n  Sufijo de la cadena v= %s\n",sufix_rev(cad1,tam),sufix_rev(cad2,tam));
			break;
	}
	printf("\nPresiona %cc%c para cambiar las cadenas, cualquier otra tecla para volver al men%c",34,34,163);
	if(getch()=='c')
		return menu_main(cad1,cad2,1);
	else
		return menu_main(cad1,cad2,0);
}

void menu_subztr(ztr cad1,ztr cad2){
	system("cls");
	puts("========\t5.- OBTENER SUBCADENA DE LAS CADENAS\t========\n\n\n");
	show_ztrs(cad1,cad2);
	int suf=0,pre=0,verify=0;
	printf("\n Introduce el n%cmero de caracteres a eliminar (de izquierda a derecha): ",163);
	verify=scanf("%d",&pre);
	getchar();			
	
	if(verify!=1)			
		return menu_subztr(cad1,cad2);
	verify=0;
	
	while(verify!=1){
		printf("\n Introduce el n%cmero de caracteres a eliminar (de derecha a izquierda): ",163);
		verify=scanf("%d",&suf);			
		getchar();
	}
	printf("\n\n  Subcadena de u= %s\n\n  Subcadena de v= %s\n",subztring(cad1,pre,suf),subztring(cad2,pre,suf));
	printf("\nPresiona %cc%c para cambiar las cadenas, cualquier otra tecla para volver al men%c",34,34,163);
	if(getch()=='c')
		return menu_main(cad1,cad2,1);
	else
		return menu_main(cad1,cad2,0);
}

void menu_subsec(ztr cad1,ztr cad2){
	system("cls");
	puts("========\t6.- OBTENER SUBSECUENCIA DE LAS CADENAS\t========\n\n\n");
	show_ztrs(cad1,cad2);
	char del;
	ztr sub1=cad1,sub2=cad2;
	while(del!='0'){
		printf("\n Ingresa el car%cter a eliminar de las cadenas ( 0 para terminar ): ",160);
		scanf("%c",&del);
		getchar();
		if(del=='0')
			break;
		sub1=erase_c(del,sub1);sub2=erase_c(del,sub2);
		printf("\n\n  Subsecuencia de la cadena u= %s",sub1);
		if(!contains(del,cad1))
			printf("\n\tEl car%cter introducido no est%c en la cadena u!",160,160);
			
		printf("\n\n  Subsecuencia de la cadena v= %s\n",sub2);
		
		if(!contains(del,cad2))
			printf("\tEl car%cter introducido no est%c en la cadena v!\n\n",160,160);
	}
	printf("\nPresiona %cc%c para cambiar las cadenas, cualquier otra tecla para volver al men%c",34,34,163);
	if(getch()=='c')
		return menu_main(cad1,cad2,1);
	else
		return menu_main(cad1,cad2,0);
}

void menu_rev(ztr cad1,ztr cad2){
	system("cls");
	puts("========\t7.- INVERTIR LAS CADENAS\t========\n\n\n");
	show_ztrs(cad1,cad2);
	printf("\n\n  Inversa de la cadena u= %s\n\n  Inversa de la cadena v= %s\n\n",reverse(cad1),reverse(cad2));
	printf("\nPresiona %cc%c para cambiar las cadenas, cualquier otra tecla para volver al men%c",34,34,163);
	if(getch()=='c')
		return menu_main(cad1,cad2,1);
	else
		return menu_main(cad1,cad2,0);
}

void menu_power(ztr cad1,ztr cad2){
	system("cls");
	puts("========\t8.- POTENCIA DE LAS CADENAS\t========\n\n\n");
	show_ztrs(cad1,cad2);int pow=0,verify=0;
	puts("\n Introduce el valor de la potencia: ");
	verify=scanf("%d",&pow);
	getchar();
	if(verify==0)
		return menu_power(cad1,cad2);
	printf("\n  Cadena u a la potencia %d = %s\n\n  Cadena v a la potencia %d = %s\n",pow,power(cad1,pow),pow,power(cad2,pow));
	printf("\nPresiona %cc%c para cambiar las cadenas, cualquier otra tecla para volver al men%c",34,34,163);
	if(getch()=='c')
		return menu_main(cad1,cad2,1);
	else
		return menu_main(cad1,cad2,0);
}

void type_ztrs(ztr cad1,ztr cad2){
	puts("  \nIntroduce la primera cadena: ");
	getztring(cad1);
	//cad1=getztring2();
	puts("");
	puts("  \nIntroduce la segunda cadena: ");
	//cad2=getztring2();
	getztring(cad2);
}

void menu_tpztrs(ztr cad1,ztr cad2){
	system("cls");
	puts("========\t1.- INTRODUCIR CADENAS\t========\n\n\n");
	type_ztrs(cad1,cad2);
	return menu_main(cad1,cad2,0);	
}

void menu_chngztrs(ztr cad1,ztr cad2){
	system("cls");
	puts("========\t1.- CAMBIAR CADENAS\t========\n\n\n");
	type_ztrs(cad1,cad2);
	//if(opt!=0)
	//	return menu_main(cad1,cad2,opt);
	return menu_main(cad1,cad2,0);	
}

void menu_main(ztr cad1,ztr cad2,int opt){
	system("cls");
	//getchar();
	printf("\t- PR%cCTICA NO. 1 -\n\n",181);
	int verify;
	if(isempty(cad1)&&isempty(cad2)){
		printf("\n Escoge una opci%cn\n",162);
		puts("\n 1.- Introducir cadenas\n");	
	}
	else{
		show_ztrs(cad1,cad2);
		printf("\n Escoge una opci%cn\n",162);
		puts("\n 1.- Cambiar cadenas\n");
	}
	puts(" 2.- Concatenar cadenas\n\n 3.- Obtener prefijo de las cadenas\n\n 4.- Obtener sufijo de las cadenas\n");
	puts(" 5.- Obtener subcadena de las cadenas\n\n 6.- Obtener subsecuencia de las cadenas\n\n 7.- Invertir las cadenas\n\n 8.- Potencia de las cadenas\n ");

	//verify=scanf("%d",&opt);
	//getchar();
	
	if(!(0<opt&&opt<9)){
		scanf("%d",&opt);
		getchar();
		return menu_main(cad1,cad2,opt);
	}
		
	//if(verify!=1)
	//	return menu_main(cad1,cad2,0);
	
	switch(opt){
		case 1:
			if(isempty(cad1)&&isempty(cad2)){
				menu_tpztrs(cad1,cad2);
			}
			else{
				menu_chngztrs(cad1,cad2);
			}	
			break;
			
		case 2:
			menu_concat(cad1,cad2);
			break;
			
		case 3:
			menu_prefix(cad1,cad2,0);
			break;
			
		case 4:
			menu_sufix(cad1,cad2,0);
			break;
			
		case 5:
			menu_subztr(cad1,cad2);
			break;
			
		case 6:
			menu_subsec(cad1,cad2);
			break;
			
		case 7:
			menu_rev(cad1,cad2);
			break;
			
		case 8:
			menu_power(cad1,cad2);
			break;
		
	}
}
