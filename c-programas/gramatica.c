#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/*
	E -> T + E
	E -> T
	T -> F * T
	T -> F
	F -> num
*/

int main (int argc,char **argv)
{
	//char cadena[50];
	int i,l;

	printf("Introduce una cadena: \n");
	//LEER CON <STRING.H>
	/*gets(palabra);
	l = strlen(palabra);
	palabra[strlen(palabra)] = '\0';
	
	puts(palabra);
	*/
	char const cadena[] = "27+89";
	//scanf("%s", cadena);
	
	
	int re = E(cadena);
	
	if(re == 1)
		printf("es valida");
	else
		printf("Es invalida");

	/*while (palabra[i] != '\0'){
		printf(" %c", &palabra[i]);
		i++;
	}
	*/

	return 0;
}

int E(char cadena[]){
	int res;
	char c;
	res = T(cadena);
	if(res == 0)
		return 0;
		
		int i;
	for(i = 0; i < cadena[ i ]!='\0'; i++ ){
		c = cadena[i];
		if(c == '+')
			return E(cadena);
		else{
				if(c != '\0'){
					printf("Valida E");
					return 1;
				}
				else {
					printf("Error: Se esperaba '*' \n");
					return 0;
				}
		}
	}
}

int T(char cadena[]){
	int res;
	char c;
	res = F(cadena);
	if (res == 0)
		return 0;
	
		int i;
		for(i = 0; i < cadena[ i ]!='\0'; i++ ){
			c = cadena[i];
			if(c == '*')
				return T(cadena);
			else{
				// c = c[strlen(cadena)-1]; devolver el ultimo caracter al flujo de entrada
				ungetc(c, stdin);
				
				if(c != '\0'){
					printf("Valida T");
					return 1;
					
				}
				else {
					printf("Error: Se esperaba '*' \n");
					return 0;
				}
			}
		}
	
}

int F(char cadena[]){
	char c;
	// c = getchar();
	int i;
	for(i = 0; i < cadena[ i ]!='\0'; i++ ){
		c = cadena[i];
		if(isdigit(c)){
			
			printf("Numero %c aceptado\n", c);
			return 1;
		}
		else{
			printf("Se esperaba un numero");
			return 0;
		}
	}
}
