#include <stack>
#include <vector>
#include <stdio.h>
#include <iostream>
using namespace std;

typedef pair<int, char> transicion;

class Nodo{
	int estado_final;
	
	public:
		void setEstadoFinal(int u){
			estado_final = u;
		}
		
		int getEstadoFinal(){
			return estado_final;
		}
		
		vector<transicion> transiciones;
		
		Nodo(void){
		}
};

class Automata{
	int numero_nodos;
	string abecedario;
	int estado_inicial;	
	string tipo_automata;
	vector< Nodo > nodos;
	
	bool Verifica(int u, int v, char tran){
		for(int i = 0; i < nodos[u].transiciones.size(); i++){
			if(nodos[u].transiciones[i].second == tran){
				printf("Recuerda, no puede haber ambiguedad en un AFD\n");
				return false;
			}
		} return true;
	}
	
	void DefineNumeroEstados(int numero_estados){
		abecedario = "";
		numero_nodos = numero_estados;
		nodos.resize(numero_estados + 2);
		for(int i = 0; i < numero_estados; i++){
			nodos[i].setEstadoFinal(0);
		}
	}
	
	void DefineAbecedario(string abecedario_recibido){
		abecedario += abecedario_recibido;
		if(tipo_automata == "AFN") abecedario += "?";
	}
	
	void DefineTransiciones(int transiciones){
		printf("Definir automatada...recuerda, el numero de nodos/estados, esta indexado desde 0\n");
		for(int i = 0; i < transiciones; i++){
			int u, v;
			char tran;
			printf("%d ) Ingresa 2 enteros, u, v, para expresar que u esta conectado con v e ingresa un caracter para definir la transicion\n>", i);
			cin >> u >> v >> tran;
			
			bool existe = false;
			for(int j = 0; j < abecedario.size(); j++){
				if(abecedario[j] == tran) existe = true;
			} if(!existe) { printf("El caracter que ingresaste no existe en el abecedario\n"); i--; continue; }
			
			if(tipo_automata == "AFD"){
				if(!Verifica(u, v, tran)){ i--; continue; }
			}
			
			nodos[u].transiciones.push_back(transicion(v, tran));
		}
	}
	
	void DefineEstadosFinales(int estados_finales){
		for(int i = 0; i < estados_finales; i++){
			int u;
			printf("%d) Ingresa un entero u, que definira que estado/nodo sera final\n>");
			cin >> u; 
			nodos[u].setEstadoFinal(1);
		}
	}
	
	void DefineEstadoInicial(int estado_inicial_param){
		estado_inicial = estado_inicial_param;
	}
	
	void DefineTipoAutomata(){
		string query;
		printf("El automata sera AFD o AFN?(AFD, AFN)\n>");
		cin >> tipo_automata;
	}

	public:
		Automata(int numero_estados, string abecedario_recibido, int transiciones, int numero_estados_finales, int est_inicial){
			DefineNumeroEstados(numero_estados);
			DefineTipoAutomata();
			DefineAbecedario(abecedario_recibido);
			DefineTransiciones(transiciones);
			DefineEstadosFinales(numero_estados_finales);
			DefineEstadoInicial(est_inicial);
		}
		
		bool VerificaCadena(string cadena){
			bool resultado = false;
			
			stack< pair<int, int> > pila; 
			pila.push( make_pair(estado_inicial, 0) );
			
			while(!pila.empty()){
				int estado_actual = pila.top().first;
				int indice = pila.top().second;
				pila.pop();
				
				cout << estado_actual << endl;
				
				if(indice == cadena.size()){
					if(nodos[estado_actual].getEstadoFinal() == 1){
						resultado = true;
					} else {
						continue;
					}
				}
			
				for(int i = 0; i < nodos[estado_actual].transiciones.size(); i++){
					if(nodos[estado_actual].transiciones[i].second == cadena[indice]){
						pila.push(make_pair(nodos[estado_actual].transiciones[i].first, indice++));
					}
					
					if(nodos[estado_actual].transiciones[i].second == '?'){
						pila.push(make_pair(nodos[estado_actual].transiciones[i].first, indice));
					}
				}
			} return resultado;
		}
};

int main(){
	Automata AFD(5, "01x", 4, 1, 1);
	if(AFD.VerificaCadena("11x")){
		cout << "SHIDA";
	}
	return 0;
}
