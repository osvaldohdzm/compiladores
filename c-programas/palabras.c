/*
AUTOR: Edgardo Adri�n Franco Mart�nez (C) Agosto 2013
VERSI�N: 1.1 

DESCRIPCI�N: Programa de ejemplo para manejar una lista din�mica de palabras de diferentes longitudes (Aprovechando la memoria din�mica)

OBSERVACIONES: 
El programa recibe en el programa principal dos cadenas de no m�s de 50 caracteres dada por el usuario. Concatena las cadenas
1 y 2 y luego las cadenas 2 y la resultante previa 3.

COMPILACI�N: gcc palabras.c -o palabras 
EJECUCI�N: palabras.exe (En Windows) - ./palabras (En Linux)
*/

//LIBRERIAS
#include<stdio.h>  	//Incluye las funciones estandar de entrada y salida
#include<string.h>	//Incluye las funciones de manejo de cadenas
#include<stdlib.h>	//Incluye las funciones de manejo de memoria din�mica

//PROGRAMA PRINCIPAL
//Estructura para un nodo de la lista de palabras
typedef struct nodo_palabra
{
	char *palabra;
	struct nodo_palabra *siguiente;	
}nodo_palabra;

//Estructura para una lista de palabras
typedef struct lista_palabras
{
	nodo_palabra *primer_palabra;
	nodo_palabra *ultima_palabra;
	int numero_palabras;
}lista_palabras;
//Funci�n para inicializar una lista L de palabras
void InicializaListaPalabras(lista_palabras *l);
//Funci�n que introduce una palabra P a una lista de palabras L
void IntroducirPalabra(char *p,lista_palabras *l);
//Funci�n que imprime en pantalla la lista de palabras L
void ImprimirPalabras(lista_palabras *l);
//Funci�n que retorna el apuntador a una palabra N de la lista de palabras L
char * DamePalabra(lista_palabras *l,int n);

//PROGRAMA PRINCIPAL
int main(void)
{
	char c[51];							//Arreglo estatico para almacenar las palabras dadas por el usuario (Maximo 50 caracteres)
	lista_palabras mi_lista;			//Lista de palabras
	char *nueva_palabra;				//Palabra auxiliar para las nuevas palabras (Desde el main me encargo de generar memoria para nuevas palabras)	
	char *palabra_aux1,*palabra_aux2;	//Palabras auxiliares 
	
	/*Inicializar la lista*/
	InicializaListaPalabras(&mi_lista);	
	
	/*Capturando una palabra del usuario*/	
	scanf("%s",c);
	nueva_palabra=malloc((strlen(c)+1)*sizeof(char));	//Medir la nueva palabra y apartar memoria
	strcpy(nueva_palabra,c);							//Copiar la palabra a la nueva palabra
	IntroducirPalabra(nueva_palabra,&mi_lista);			//Introducir la nueva palabra a mi lista de palabras

	/*Capturando una segunda palabra del usuario*/	
	scanf("%s",c);
	nueva_palabra=malloc((strlen(c)+1)*sizeof(char));	//Medir la nueva palabra y apartar memoria
	strcpy(nueva_palabra,c);							//Copiar la palabra a la nueva palabra
	IntroducirPalabra(nueva_palabra,&mi_lista);						//Introducir la nueva palabra a mi lista de palabras
	
	/*Pidiendo las dos primeras palabras de la lista para concatenarlas y agregar la concatenaci�n a la lista*/
	palabra_aux1=DamePalabra(&mi_lista,1);
	palabra_aux2=DamePalabra(&mi_lista,2);
	
	nueva_palabra=malloc((strlen(palabra_aux1)+strlen(palabra_aux2)+1)*sizeof(char));	//Espacio para la nueva palabra (Concatenada)

	strcpy(nueva_palabra,palabra_aux1);			//Copiar la palabra 1 a la nueva
	strcat(nueva_palabra,palabra_aux2);			//Concatenar la nueva palabra (palabra 1) con la palabra 2
	IntroducirPalabra(nueva_palabra,&mi_lista);	//Introducir a la lista de palabras la palabra concatenada


	/*Pidiendo las ultimas dos palabras de la lista para concatenarlas y agregar la concatenaci�n a la lista*/
	palabra_aux1=DamePalabra(&mi_lista,2);
	palabra_aux2=DamePalabra(&mi_lista,3);
	
	nueva_palabra=malloc((strlen(palabra_aux1)+strlen(palabra_aux2)+1)*sizeof(char));	//Espacio para la nueva palabra (Concatenada)

	strcpy(nueva_palabra,palabra_aux1);			//Copiar la palabra 1 a la nueva
	strcat(nueva_palabra,palabra_aux2);			//Concatenar la nueva palabra (palabra 1) con la palabra 2
	IntroducirPalabra(nueva_palabra,&mi_lista);	//Introducir a la lista de palabras la palabra concatenada

	ImprimirPalabras(&mi_lista);					//Imprimir mi_lista de palabras
	return 0;
}

/*
void InicializaListaPalabras(lista_palabras *l);
Descripci�n: Funci�n para inicializar una lista L de palabras
Recibe: lista_palabras *l (Referencia a una lsita de palabras L a operar)
Devuelve:
Observaciones: El usuario a creado una lista_palabras y L tiene la referencia a ella.
*/
void InicializaListaPalabras(lista_palabras *l)
{
	l->primer_palabra=NULL;
	l->ultima_palabra=NULL;
	l->numero_palabras=0;
}

/*
void IntroducirPalabra(char *p,lista_palabras *l);
Descripci�n: Funci�n que introduce una palabra P a una lista de palabras L
Recibe: char *p(Referencia a la palabra) y lista_palabras *l (Referencia a una lista de palabras L a operar)
Devuelve:
Observaciones: El usuario a creado una lista_palabras, L tiene la referencia a ella, la lists L ha sido inicializada y P tiene la direcci�n de donde esta alamcenada la palabra.
NOTA: La palabra p solo se referencia el usuario es responsable que esta ya este en memoria y no se elimine sin notificar a la lista
*/
void IntroducirPalabra(char *p,lista_palabras *l)
{
	//Crear el nodo nuevo (nodo_palabra)
	nodo_palabra *nuevo;
	nuevo=malloc(sizeof(nodo_palabra));	//Memoria asiganada al nuevo nodo
	nuevo->siguiente=NULL;				//Nuevo nodo apunta a NULL en siguiente
	
	//Introducir la palabra al nodo_palabra
	nuevo->palabra=p;

	//Si es la primer palabra de la lista
	if (l->primer_palabra==NULL)
	{
		l->primer_palabra=nuevo;
		l->ultima_palabra=nuevo;
	}
	//Si hay m�s palabras ya en la lista
	else
	{
		l->ultima_palabra->siguiente=nuevo;	
		l->ultima_palabra=nuevo;
	}
	
	//Incrementar el conteo de las palabra en la lista
	l->numero_palabras++;	
	return;
}

/*
void ImprimirPalabras(lista_palabras *l)
Descripci�n: Funci�n que imprime en pantalla la lista de palabras L
Recibe: lista_palabras *l (Referencia a una lista de palabras L a imprimir)
Devuelve:
Observaciones: El usuario a creado una lista_palabras, L tiene la referencia a ella, la lista L ha sido inicializada.
*/
void ImprimirPalabras(lista_palabras *l)
{
	int i;
	nodo_palabra *aux=l->primer_palabra;
		
	printf("\n*******************************");	
	for(i=1;aux!=NULL;i++)
	{
		printf("\n%d\t%s",i,aux->palabra);
		aux=aux->siguiente;
	}
	printf("\n*******************************\n");	
	return;
}

/*
char *DamePalabra(lista_palabras *l,int n);
Descripci�n: Funci�n que retorna el apuntador a una palabra N de la lista de palabras L
Recibe: lista_palabras *l (Referencia a una lista de palabras L a operar), int n(N�mero de palabra que se desea comienza en 1)
Devuelve: char * (Apuntador a la palabra encontrada en a posici�n N)
Observaciones: El usuario a creado una lista_palabras, L tiene la referencia a ella, la lista L ha sido inicializada.
NOTA:Si la palabra n solo se encuentra se retorna un apuntador a NULL
*/
char *DamePalabra(lista_palabras *l,int n)
{
	int i;	
	char *p=NULL; //Si no se encuentra la palabra retorna NULL
	nodo_palabra *aux=l->primer_palabra;
	
	//Saltar en la lista hasta llegar a la palabra
	for(i=1;aux!=NULL&&i<n;i++) //Si se llega a un nodo NULL o a la palabra buscada dejar de saltar
	{
		aux=aux->siguiente;
	}
	//Si el nodo al que llego no es nulo retornar la palabra
	if(aux!=NULL)
		p=aux->palabra;
	
	return p;
}
