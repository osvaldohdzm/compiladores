#include "AFN.hpp"
	void token(char* str) {   
    char * pch;

    pch = strtok (str,"ESCRIBIR_TODA_LA_EXPRECION_REGULAR_ENTRE_PARENTESIS_EXPRECION_REGULAR : ,");
  
    while (pch != NULL)
    {
 
    pch = strtok (NULL, " ESCRIBIR_TODA_LA_EXPRECION_REGULAR_ENTRE_PARENTESIS_EXPRECION_REGULAR : , - ");

    } 


    }  

int main() {
	cout<<"\n\nAlgoritmo de construccion de Thompson mediante una expresion regular como entrada "
		<<"y devuelve su correspondiente Automata Finito no determinista\n\n";
	cout<<"\n\nLos elementos basicos para la construccion de la AFN son : \n";
	

	AFN a, b;

	cout<<"\nPara el segmento de expresion regular : (a)";
	a.set_vertex(2);
	a.set_transition(0, 1, 'a');
	a.set_final_estado(1);
	a.display();


	cout<<"\nPara el segmento de expresion regular : (b)";
	b.set_vertex(2);
	b.set_transition(0, 1, 'b');
	b.set_final_estado(1);
	b.display();


	cout<<"\nPara el segmento de expresion regular [ Concatenacion ] : (a.b)";
	AFN ab = concat(a, b);
	ab.display();


	cout<<"\nPara el segmento de expresion regular [ Cerradura de Kleene  ] : (a*)";
	AFN a_star = kleene(a);
	a_star.display();


	cout<<"\nPara el segmento de expresion regular [ Cerradura de Kleene  ] : (a+)";
	AFN b_star = Positiva(a);
	b_star.display();

	cout<<"\nPara el segmento de expresion regular [Or] : (a|b)";
	int no_of_seleccions;
	no_of_seleccions = 2;
	vector<AFN> seleccions(no_of_seleccions, AFN());
	seleccions.at(0) = a;
	seleccions.at(1) = b;
	AFN a_or_b = or_seleccion(seleccions, no_of_seleccions);
	a_or_b.display();	




	FILE *fp;
 		char buffer[100000];
 		fp = fopen ( "ExprecionRegular.txt", "r+" );
 		fscanf(fp, "%s" ,buffer);
 	  	cout << buffer << endl;
 	  	 
 		token(buffer);
 		fclose ( fp );
	AFN example_AFN = re_to_AFN(buffer);  //-----------------------------leer expresion regular
	
		cout<<"Por ejemplo : \n Para la expresion regular "<<buffer<<" -- \n"<<endl;
	




	example_AFN.display();
	


	return 0;
}


// ESCRIBIR_TODA_LA_EXPRECION_REGULAR_ENTRE_PARENTESIS:EXPRECION_REGULAR:((a|(c.d+)))   // expresion regular como las recibe
