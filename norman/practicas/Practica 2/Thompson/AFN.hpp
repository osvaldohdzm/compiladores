#ifndef __AFN_HPP__
#define __AFN_HPP__
#include<iostream>
#include<vector>
#include<string>
#include<set>
#include<stack>
#include <stdio.h>
#include <string.h>
#include <cstdlib>



using namespace std;
//Estructura
struct trans {
	int vertex_from;
	int vertex_to;
	char trans_simbolo;
};


class AFN {

public:
	vector<int> vertex;
	vector<trans> trancisiones;
	int final_estado;

	AFN() {

	}

	int get_vertex_count() {
		return vertex.size();
	}

	void set_vertex(int no_vertex) {
		for(int i = 0; i < no_vertex; i++) {
			vertex.push_back(i);
		}
	}

	void set_transition(int vertex_from, int vertex_to, char trans_simbolo) {
		trans new_trans;
		new_trans.vertex_from = vertex_from;
		new_trans.vertex_to = vertex_to;
		new_trans.trans_simbolo = trans_simbolo;
		trancisiones.push_back(new_trans);
	}

	void set_final_estado(int fs) {
		final_estado = fs;
	}

	int get_final_estado() {
		return final_estado;
	}

	void display() {
		trans new_trans;
		cout<<"\n";
		for(int i = 0; i < trancisiones.size(); i++) {
			new_trans = trancisiones.at(i);
			cout<<"q"<<new_trans.vertex_from<<" --> q"<<new_trans.vertex_to<<"  : Simbolo - "<<new_trans.trans_simbolo<<endl;
		}
		cout<<"\nEl estado inicial es q0"<<endl;
		cout<<"\nEl estado final es q"<<get_final_estado()<<endl;
	}
	


};
// Concatencaion
//  concatenacion
AFN concat(AFN a, AFN b) {
	AFN resultado;
	resultado.set_vertex(a.get_vertex_count() + b.get_vertex_count());
	int i;
	trans new_trans;

	for(i = 0; i < a.trancisiones.size(); i++) {
		new_trans = a.trancisiones.at(i);
		resultado.set_transition(new_trans.vertex_from, new_trans.vertex_to, new_trans.trans_simbolo);
	}

	resultado.set_transition(a.get_final_estado(), a.get_vertex_count(), 'e');   //Estado Epsilon

	for(i = 0; i < b.trancisiones.size(); i++) {
		new_trans = b.trancisiones.at(i);
		resultado.set_transition(new_trans.vertex_from + a.get_vertex_count(), new_trans.vertex_to + a.get_vertex_count(), new_trans.trans_simbolo);
	}

	resultado.set_final_estado(a.get_vertex_count() + b.get_vertex_count() - 1);

	return resultado;
}

//operacion cerradura kleene *
AFN kleene(AFN a) {
	AFN resultado;
	int i;
	trans new_trans;
	
	resultado.set_vertex(a.get_vertex_count() + 2);

	resultado.set_transition(0, 1, 'e');   //Estado Epsilon

	for(i = 0; i < a.trancisiones.size(); i++) {
		new_trans = a.trancisiones.at(i);
		resultado.set_transition(new_trans.vertex_from + 1, new_trans.vertex_to + 1, new_trans.trans_simbolo);
	}

	resultado.set_transition(a.get_vertex_count(), a.get_vertex_count() + 1, 'e'); //Estado Epsilon
	resultado.set_transition(a.get_vertex_count(), 1, 'e');  //Estado Epsilon
	resultado.set_transition(0, a.get_vertex_count() + 1, 'e'); //Esatdo Epsilon

	resultado.set_final_estado(a.get_vertex_count() + 1);

	return resultado;
}
//operacion +
AFN Positiva(AFN a) {
	AFN resultado;
	int i;
	trans new_trans;
	
	resultado.set_vertex(a.get_vertex_count() + 2);

	resultado.set_transition(0, 1, 'e');   //Estado Epsilon

	for(i = 0; i < a.trancisiones.size(); i++) {
		new_trans = a.trancisiones.at(i);
		resultado.set_transition(new_trans.vertex_from + 1, new_trans.vertex_to + 1, new_trans.trans_simbolo);
	}

	resultado.set_transition(a.get_vertex_count(), a.get_vertex_count() + 1, 'e'); //Estado Epsilon
	resultado.set_transition(a.get_vertex_count(), 1, 'e');  //Estado Epsilon
//	resultado.set_transition(0, a.get_vertex_count() + 1, 'e'); //Esatdo Epsilon

	resultado.set_final_estado(a.get_vertex_count() + 1);

	return resultado;
}

// operacion OR
AFN or_seleccion(vector<AFN> seleccions, int no_of_seleccions) {
	AFN resultado;
	int vertex_count = 2;
	int i, j;
	AFN med;
	trans new_trans;

	for(i = 0; i < no_of_seleccions; i++) {
		vertex_count += seleccions.at(i).get_vertex_count();
	}

	resultado.set_vertex(vertex_count);
	
	int adder_track = 1;

	for(i = 0; i < no_of_seleccions; i++) {
		resultado.set_transition(0, adder_track, 'e');  // Estado Epsilon
		med = seleccions.at(i);
		for(j = 0; j < med.trancisiones.size(); j++) {
			new_trans = med.trancisiones.at(j);
			resultado.set_transition(new_trans.vertex_from + adder_track, new_trans.vertex_to + adder_track, new_trans.trans_simbolo);
		}
		adder_track += med.get_vertex_count();

		resultado.set_transition(adder_track - 1, vertex_count - 1, 'e'); // Esatdo Epsilon
	}

	resultado.set_final_estado(vertex_count - 1);

	return resultado;
}

//Pila de simbolos
AFN re_to_AFN(string re) {
	stack<char> operador;
	stack<AFN> operands;
	char op_sym;
	int op_count;
	char cur_sym;
	AFN *new_sym;
	
	for(string::iterator it = re.begin(); it != re.end(); ++it) {
		cur_sym = *it;
		if(cur_sym != '(' && cur_sym != ')' && cur_sym != '*' && cur_sym != '+' && cur_sym != '|' && cur_sym != '.') {
			new_sym = new AFN();
			new_sym->set_vertex(2);
			new_sym->set_transition(0, 1, cur_sym);
			new_sym->set_final_estado(1);
			operands.push(*new_sym);
			delete new_sym;
		} else {
			if(cur_sym == '*') {
				AFN star_sym = operands.top();
				operands.pop();
				operands.push(kleene(star_sym));
			} else if(cur_sym == '+') {
				AFN star_sym = operands.top();
				operands.pop();
				operands.push(Positiva(star_sym));                 
			} else if(cur_sym == '.') {
				operador.push(cur_sym);
			} else if(cur_sym == '|') {
				operador.push(cur_sym);
			} else if(cur_sym == '(') {
				operador.push(cur_sym);
			} else {
				op_count = 0;
				char c;
				op_sym = operador.top();
				if(op_sym == '(') continue;
				do {
					operador.pop();
					op_count++;
				} while(operador.top() != '(');
				operador.pop();
				AFN op1;
				AFN op2;
				vector<AFN> seleccions;
				if(op_sym == '.') {
					for(int i = 0; i < op_count; i++) {
						op2 = operands.top();
						operands.pop();
						op1 = operands.top();
						operands.pop();
						operands.push(concat(op1, op2));
					}
				} else if(op_sym == '|'){
					seleccions.assign(op_count + 1, AFN());
					int tracker = op_count;
					for(int i = 0; i < op_count + 1; i++) {
						seleccions.at(tracker) = operands.top();
						tracker--;
						operands.pop();
					}
					operands.push(or_seleccion(seleccions, op_count+1));
				} else {
					
				}
			}
		}
	}

	return operands.top();
}


#endif
