#ifndef AFND_HPP
#define	AFND_HPP

#define AFN_MAX 11
#define EPSILON -1
#define ACEPTA -2
#define MAXIMO 80

#include "tupla.hpp"
#include <iostream>
#include <sstream> 
#include <iomanip>
#include <stdlib.h>
#include <string.h>
#include <stack>
#include<vector>



using namespace std;

class AFND {
public:
    AFND(vector<string> edosf,string alfabeto);
    string itos(int);
    int metodoSubConjuntosAFNaAND(int aristas[][AFN_MAX]);
    void setAlfabeto(string alfabeto);
    void setEstadosFinalesAFD();
    void setEstadosFinalesAFND(vector<string> edosf);
    vector<tupla*> getTuplasAFD();
    vector<string> getEdosFinalesAFD();
    
private:
    vector <tupla*> tuplasAFD;
    vector<string> edosAFD;
    vector<string> edosFinalesAFD;
    vector<string> edosFinalesAFND;
    string alfabeto;
    
    void e_clausura(int edo, int* s, int n, int* a, int &na, int aristas[][AFN_MAX]);
    void mover(int* a, int na, int* t, int &m, int c, int aristas[][AFN_MAX]);
    void print(int edo, int* t, int m, char c);
    void copy(int* t, int m, int estadosD[][AFN_MAX], int &nest, int* nD);
    bool no_marcado(int* t, int m, int estadosD[][AFN_MAX], int nest);
    void aceptacion(int aristas[][AFN_MAX]);
    string getEdosSig(int*edos, int m);
    void print_estadosD(int estadosD[][AFN_MAX], int nest, int nD[]);
    void setVectorEdosAFD(int estadosD[][AFN_MAX], int nest, int nD[]);
    void corregirEdosTuplas();
    void corregirEdosFinalesAFD();
    void elimiarIgualesEdosFinAFD();

};

#endif	/* AFND_HPP */

