#include "AFND.hpp"

AFND::AFND(vector<string> edosf, string alfabeto) {
    edosFinalesAFND = edosf;
    this->alfabeto = alfabeto;
}

void AFND::setAlfabeto(string alfabeto) {
    this->alfabeto = alfabeto;
}

int AFND::metodoSubConjuntosAFNaAND(int aristas[][AFN_MAX]) {
    int m, i, j, k;
    int t[AFN_MAX];
     int estadosD[AFN_MAX][AFN_MAX];
    int nest = 0;
    int nD[AFN_MAX];
    int a[AFN_MAX], na;
    *t = 0;
     m = 1;
     
    cout << "estado : " << nest << endl;

    e_clausura(nest, t, m, a, na, aristas);
    copy(a, na, estadosD, nest, nD);

    for (i = 0; i < alfabeto.length(); i++) {
        mover(a, na, t, m, alfabeto.at(i), aristas);
        cout << "estado : " << nest << endl;

        string simbolo = "";
        simbolo += alfabeto.at(i);

        if (m != 0)
            tuplasAFD.push_back(new tupla("0", simbolo, getEdosSig(t, m)));
        cout << "trans " << 0 << ", " << alfabeto.at(i) << "," << getEdosSig(t, m) << endl;

        print(0, t, m, alfabeto.at(i));
        copy(t, m, estadosD, nest, nD);
    }

    for (k = 1; k < nest; k++) {
        for (j = 0; j < nD[k]; j++)
            t[j] = estadosD[k][j];

        m = nD[k];
        cout << " K : " << k << endl;
        e_clausura(k, t, m, a, na, aristas);
        for (i = 0; i < alfabeto.length(); i++) {
            string simbolo = "";

            mover(a, na, t, m, alfabeto.at(i), aristas);
            if (m) {
                if (no_marcado(t, m, estadosD, nest)) {
                    cout << "estado : " << nest << endl;
                    copy(t, m, estadosD, nest, nD);
                    simbolo += alfabeto.at(i);
                    if (m != 0)
                        tuplasAFD.push_back(new tupla(itos(k), "" + simbolo, getEdosSig(t, m)));

                    cout << "trans " << k << ", " << alfabeto.at(i) << "," << getEdosSig(t, m) << endl;

                    print(k, t, m, alfabeto.at(i));
                } else {
                    simbolo += alfabeto.at(i);
                    if (m != 0)
                        tuplasAFD.push_back(new tupla(itos(k), "" + simbolo + "", getEdosSig(t, m)));
                    cout << "trans " << k << ", " << alfabeto.at(i) << "," << getEdosSig(t, m) << endl;

                    print(k, t, m, alfabeto.at(i));
                }
            }
        }
    }


    aceptacion(aristas);
    print_estadosD(estadosD, nest, nD);


    setVectorEdosAFD(estadosD, nest, nD);
    setEstadosFinalesAFD();
    corregirEdosTuplas();
    corregirEdosFinalesAFD();
    elimiarIgualesEdosFinAFD();


    cout << "Nuevas transiciones para automata AFD" << endl;

    for (int t = 0; t < edosFinalesAFD.size(); t++) {
        cout << "Estado Final:" << edosFinalesAFD.at(t) << endl;
    }

    for (int t = 0; t < tuplasAFD.size(); t++) {
        cout << "(" << tuplasAFD.at(t)->getEstadoI() << "," << tuplasAFD.at(t)->getTrancicion() << "," << tuplasAFD.at(t)->getEstadoF() << ")" << endl;
    }

    cout << "---------------------------------------" << endl << endl;
    return 0;
}

void AFND::corregirEdosTuplas() {
    for (int t = 0; t < tuplasAFD.size(); t++) {
        for (int edoC = 0; edoC < edosAFD.size(); edoC++) {
            if (tuplasAFD.at(t)->getEstadoF() == edosAFD.at(edoC)) {
                tuplasAFD.at(t)->setEstadoF(itos(edoC));
            }
        }

    }
}

vector<tupla*> AFND::getTuplasAFD() {
    return tuplasAFD;
}

vector<string> AFND::getEdosFinalesAFD() {
    return edosFinalesAFD;
}

void AFND::corregirEdosFinalesAFD() {
    for (int t = 0; t < edosFinalesAFD.size(); t++) {
        for (int edoC = 0; edoC < edosAFD.size(); edoC++) {
            if (edosFinalesAFD.at(t) == edosAFD.at(edoC)) {
                edosFinalesAFD.at(t) = itos(edoC);
            }
        }

    }
}

void AFND::elimiarIgualesEdosFinAFD() {
    vector<string> edosArreglado;
    bool agregar = false;
    for (int i = 0; i < edosFinalesAFD.size(); i++) {

        if (i == 0) {
            agregar = true;
        } else {
            for (int j = 0; j < edosArreglado.size(); j++) {
                if (edosArreglado.at(j) == edosFinalesAFD.at(i)) {
                    agregar = false;
                    break;
                } else {
                    agregar = true;
                }
            }
        }
        if (agregar)
            edosArreglado.push_back(edosFinalesAFD.at(i));
    }

    edosFinalesAFD = edosArreglado;
}

void AFND::setEstadosFinalesAFD() {
    for (int t = 0; t < tuplasAFD.size(); t++) {
        for (int edofAFN = 0; edofAFN < edosFinalesAFND.size(); edofAFN++) {

            if ((tuplasAFD.at(t)->getEstadoF().find("(" + edosFinalesAFND.at(edofAFN) + ")") != string::npos)
                    || tuplasAFD.at(t)->getEstadoF().find("(" + edosFinalesAFND.at(edofAFN) + ",") != string::npos
                    || tuplasAFD.at(t)->getEstadoF().find("," + edosFinalesAFND.at(edofAFN) + ",") != string::npos
                    || tuplasAFD.at(t)->getEstadoF().find("," + edosFinalesAFND.at(edofAFN) + ")") != string::npos) {
                edosFinalesAFD.push_back(tuplasAFD.at(t)->getEstadoF());
            }
        }

    }
}

void AFND::setEstadosFinalesAFND(vector<string> edosf) {
    edosFinalesAFND = edosf;
}

void AFND::print_estadosD(int estadosD[][AFN_MAX], int nest, int nD[]) {
    register int i, j;
    //clrscr();
    cout << endl << " AFD         AFN " << endl;
    cout << "---------------------------------------" << endl;
    for (i = 0; i < nest; i++) {
        cout << setw(4) << i << " : ";
        for (j = 0; j < nD[i]; j++)
            cout << setw(4) << estadosD[i][j];
        cout << endl;
    }
    cout << "---------------------------------------" << endl;

}

void AFND::setVectorEdosAFD(int estadosD[][AFN_MAX], int nest, int nD[]) {
    register int i, j;
    string ed = "";


    for (i = 0; i < nest; i++) {
        stringstream stream;
        ed = "(";

        for (j = 0; j < nD[i] - 1; j++) {

            ed += itos(estadosD[i][j]);
            ed += ",";
        }

        stream << estadosD[i][nD[i] - 1];
        ed += itos(estadosD[i][nD[i] - 1]) + ")";
        edosAFD.push_back(ed);
    }


}

void AFND::aceptacion(int aristas[][AFN_MAX]) {
    cout << "estados de aceptacion tienen los nodos : ";
    for (int i = 0; i < AFN_MAX; i++)
        if (aristas[i][i] == ACEPTA)
            cout << setw(4) << i;

}

void AFND::e_clausura(int edo, int* s, int n, int* a, int &na, int aristas[][AFN_MAX]) {
    int i, j, t, u;

    stack<int> p;

    // meter todos los estados en pila
    // inicializar cerradura a

    na = 0;

    for (i = 0; i < n; i++) {
        p.push(s[i]);
        a[i] = s[i];
        na++;
    }

    while (!p.empty()) {
        t = p.top();
        p.pop();
        for (u = 0; u < AFN_MAX; u++)
            if (aristas[t][u] == EPSILON) {
                i = 0;
                while (i < na && u != a[i])
                    i++;
                if (i == na) {
                    // anadir u a cerradura
                    a[na++] = u;
                    // meter u a pila
                    p.push(u);
                }
            }
    }
    cout << " edo T " << edo << " :";
    for (j = 0; j < na; j++)
        cout << setw(4) << a[j];
    cout << endl;
}

void AFND::print(int edo, int* t, int m, char c) {
    int j;
    cout << " mover(T" << edo << "," << c << ") : ";
    for (j = 0; j < m; j++)
        cout << setw(4) << t[j];
    cout << endl;
}

void AFND::copy(int* t, int m, int estadosD[][AFN_MAX], int &nest, int* nD) {
    int i;
    for (i = 0; i < m; i++)
        estadosD[nest][i] = t[i];
    nD[nest] = m;
    ++nest;
}

void AFND::mover(int* a, int na, int* t, int &m, int c, int aristas[][AFN_MAX]) {
    int i, j, k;
    m = 0;
    for (i = 0; i < na; i++) {
        k = a[i];
        for (j = 0; j < AFN_MAX; j++)
            if (aristas[k][j] == c)
                t[m++] = j;
    }
}

bool AFND::no_marcado(int* t, int m, int estadosD[][AFN_MAX], int nest) {
    int k = 0, j, i;
    for (k = 0; k < nest; k++) {
        i = 0;
        for (j = 0; j < m; j++)
            if (t[j] == estadosD[k][j])
                i++;
        if (i == m)
            return false;
    }
    return true;
}

string AFND::getEdosSig(int* edos, int m) {

    if (m == 0) {
        return "-";
    }

    string ed = "(";
    for (int j = 0; j < m - 1; j++) {
        stringstream stream;
        stream << edos[j];

        if (stream.str() != " " || stream.str() != "")
            ed += stream.str() + ",";
    }

    stringstream stream1;
    stream1 << edos[m - 1];
    ed += stream1.str() + ")";
    return ed;
}

string AFND::itos(int entero) {
    stringstream stream1;
    stream1 << entero;
    return stream1.str();
}



