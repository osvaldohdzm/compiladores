#include "AFND.hpp"
#include "AFD.hpp"
#include <iostream>
#include <cstring>
#include <fstream>
#include <stdlib.h>
#include <algorithm>

using namespace std;

void reconocerAFD(char* ruta, string cadena);
void reconocerAFND(char* ruta, string cadena);
string getEdoIncialArchivo(char* ruta);
vector<string> getEdoFinalArchivo(char* ruta);
string getAlfabetoArchivo(char* ruta);
string generarCadenaAFD(char* ruta);
string generarCadenaAFND(char* ruta);
vector<tupla*> getTuplasAFDArchivo(vector<tupla*> tuplas, char* ruta);
void getyLlenarMatrizAFN(char* ruta);

/*
int aristas[][AFN_MAX] =
// 0   1  2  3  4  5  6  7  8   9  10  edo sig
{   0 -1 0 0 0 0 0 -1 0 0 0
    0 0 -1 0 -1 0 0 0 0 0 0
    0 0 0 a 0 0 0 0 0 0 0
    0 0 0 0 0 0 -1 0 0 0 0
    0 0 0 0 0 b 0 0 0 0 0
    0 0 0 0 0 0 -1 0 0 0 0
    0 -1 0 0 0 0 0 -1 0 0 0
    0 0 0 0 0 0 0 0 a 0 0
    0 0 0 0 0 0 0 0 0 b 0
    0 0 0 0 0 0 0 0 0 0 b
    0 0 0 0 0 0 0 0 0 0 -2};

 */

/*  edo1(edo ini)---> 'a' -->edo2(edo sig)
 int aristas[renglon][columnas]=
 * {|edos sguientes
 * -|-----------------------------------
 * e|
 * d|     simbolo de la transicion 
 * o|     (empsilon=-1,edoFinal=-2 alfabeto=a,b,c..)
 * s|
 *  |
 * i|
 * n|
 * i|
 * }
 
 
 */

/*int aristas[][11] =
//  0   1  2  3  4  5  6  7  8   9  10  edo sig
   {0, 'a', 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, -1, -1, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 'a', 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 'b', 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 'a', 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, -2, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};*/


int aristas[][11] =
        //  0  1  2  3  4  5  6  7  8  9  10  edo sigs
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

int main(int argc, char* argv[]) {

    if (argc == 1) {
        cout << "Ingresa los paramatros al programa\nFin del programa." << endl;
        return 0;
    }

    char* ruta = argv[1];
    string argumento = argv[2];

    if ("AFD" == argumento) {
        argumento = argv[3];
        
        if (argumento == "reconocer") {
            cout << "Automata AFD modo Reconocer cadenas" << endl;
            argumento = argv[4];
            reconocerAFD(ruta, argumento);
            return 0;
            
        } else if (argumento == "generar") {
            cout << "Automata AFD modo Generar cadena" << endl;
            cout <<"Cadena generada: "<< generarCadenaAFD(ruta) << endl;
        }
        
    } else if (argumento == "AFND") {
        argumento = argv[3];
        if (argumento == "reconocer") {
            cout << "Automata AFND modo Reconocer cadenas" << endl;
            argumento = argv[4];
            reconocerAFND(ruta, argumento);
            return 0;
            
        } else if (argumento == "generar") {
            cout << "Automata AFND modo Generar cadena" << endl;
            cout <<"Cadena generada: "<< generarCadenaAFND(ruta) << endl;
        }

    }

    return 0;

}

void reconocerAFD(char* ruta, string cadena) {
    vector<string>edosFinalesAFD;
    vector<tupla*> tuplas;
    vector<string> edosFdelArchivo;
    edosFdelArchivo = getEdoFinalArchivo(ruta);

    for (int i = 1; i < edosFdelArchivo.size(); i++)
        edosFinalesAFD.push_back(edosFdelArchivo.at(i));

    cout << "Estado Incial: " << getEdoIncialArchivo(ruta) << endl;
    cout << "Estado(s)  Final: ";

    for (int i = 0; i < edosFinalesAFD.size(); i++)
        cout << edosFinalesAFD.at(i) << ", ";


    cout << "\nAlfabeto:      " << getAlfabetoArchivo(ruta) << endl << endl;


    AFD *automata = new AFD(getEdoIncialArchivo(ruta), edosFinalesAFD);


    //(EdoI,elemnto,edoF)
    tuplas = getTuplasAFDArchivo(tuplas, ruta);

    for (int tt = 0; tt < tuplas.size(); tt++)
        automata->agregaTupla(tuplas.at(tt));


    //acepta las cadenas que tinen un numero par de 1 y 0
    automata->setCadena(cadena);


    if (automata->validarCadena())
        cout << "La cadena es valida" << endl;
    else
        cout << "Error: La cadena no es valida" << endl;


}

void reconocerAFND(char* ruta, string cadena) {
    vector<string> edosf;
    vector<string> edosFdelArchivo;
    edosFdelArchivo = getEdoFinalArchivo(ruta);

    for (int i = 1; i < edosFdelArchivo.size(); i++)
        edosf.push_back(edosFdelArchivo.at(i));


    cout << "Estado Incial: " << getEdoIncialArchivo(ruta) << endl;
    cout << "Estado(s)  Final: ";

    for (int i = 0; i < edosf.size(); i++)
        cout << edosf.at(i) << ", ";

    cout << "\nAlfabeto:      " << getAlfabetoArchivo(ruta) << endl << endl;

    getyLlenarMatrizAFN(ruta);


    AFND *afn = new AFND(edosf, getAlfabetoArchivo(ruta));

    cout << endl << "Convirtiendo AFN a AFD por SubConjuntos..." << endl << endl;
    afn->metodoSubConjuntosAFNaAND(aristas);


    //-----------------------------Empieza la construcion del AFD-------------------------------------------
    vector<tupla*> tuplasAFD;
    vector<string> edosFinalesAFDdeANF;
    tuplasAFD = afn->getTuplasAFD();

    edosFinalesAFDdeANF = afn->getEdosFinalesAFD();

    AFD* automataAFD = new AFD("0", edosFinalesAFDdeANF);

    for (int tt = 0; tt < tuplasAFD.size(); tt++)
        automataAFD->agregaTupla(tuplasAFD.at(tt));

    automataAFD->setCadena(cadena);


    if (automataAFD->validarCadena())
        cout << "La cadena es valida" << endl;
    else
        cout << "Error: La cadena no es valida" << endl;

}

string generarCadenaAFD(char* ruta) {
    vector<string>edosFinalesAFD;
    vector<tupla*> tuplas;
    vector<string> edosFdelArchivo;
    edosFdelArchivo = getEdoFinalArchivo(ruta);

    for (int i = 1; i < edosFdelArchivo.size(); i++)
        edosFinalesAFD.push_back(edosFdelArchivo.at(i));

    cout << "Estado Incial: " << getEdoIncialArchivo(ruta) << endl;
    cout << "Estado(s)  Final: ";

    for (int i = 0; i < edosFinalesAFD.size(); i++)
        cout << edosFinalesAFD.at(i) << ", ";


    cout << "\nAlfabeto:      " << getAlfabetoArchivo(ruta) << endl << endl;


    AFD *automata = new AFD(getEdoIncialArchivo(ruta), edosFinalesAFD);


    //(EdoI,elemnto,edoF)
    tuplas = getTuplasAFDArchivo(tuplas, ruta);

    for (int tt = 0; tt < tuplas.size(); tt++)
        automata->agregaTupla(tuplas.at(tt));


    cout << "\n\nGenerando cadena...\n\n";
    return automata->generarCadena();

}

string generarCadenaAFND(char* ruta) {
    vector<string> edosf;
    vector<string> edosFdelArchivo;
    edosFdelArchivo = getEdoFinalArchivo(ruta);

    for (int i = 1; i < edosFdelArchivo.size(); i++)
        edosf.push_back(edosFdelArchivo.at(i));


    cout << "Estado Incial: " << getEdoIncialArchivo(ruta) << endl;
    cout << "Estado(s)  Final: ";

    for (int i = 0; i < edosf.size(); i++)
        cout << edosf.at(i) << ", ";

    cout << "\nAlfabeto:      " << getAlfabetoArchivo(ruta) << endl << endl;

    getyLlenarMatrizAFN(ruta);


    AFND *afn = new AFND(edosf, getAlfabetoArchivo(ruta));

    cout << endl << "Convirtiendo AFN a AFD por SubConjuntos..." << endl << endl;
    afn->metodoSubConjuntosAFNaAND(aristas);


    //-----------------------------Empieza la construcion del AFD-------------------------------------------
    vector<tupla*> tuplasAFD;
    vector<string> edosFinalesAFDdeANF;
    tuplasAFD = afn->getTuplasAFD();

    edosFinalesAFDdeANF = afn->getEdosFinalesAFD();

    AFD* automataAFD = new AFD("0", edosFinalesAFDdeANF);

    for (int tt = 0; tt < tuplasAFD.size(); tt++)
        automataAFD->agregaTupla(tuplasAFD.at(tt));

    cout << "\n\nGenerando cadena...\n\n";
    return automataAFD->generarCadena();

}

string getEdoIncialArchivo(char* ruta) {

    ifstream f(ruta, ifstream::in);
    char linea[128];

    if (f.fail())
        cerr << "Error al abrir el archivo Pruebas.txt" << endl;
    else {
        f.getline(linea, sizeof (linea));


        string cadena(linea);

        string buf; // Have a buffer string
        stringstream ss(cadena); // Insert the string into a stream

        vector<string> tokens; // Create vector to hold our words

        while (ss >> buf)
            tokens.push_back(buf);

        return tokens.at(1);
        f.close();


    }


}

vector<string> getEdoFinalArchivo(char* ruta) {
    vector<string> tokens;
    ifstream f(ruta, ifstream::in);
    char linea[128];

    if (f.fail())
        cerr << "Error al abrir el archivo Pruebas.txt" << endl;
    else {
        f.getline(linea, sizeof (linea));
        f.getline(linea, sizeof (linea));
        string cadena(linea);

        string buf;
        stringstream ss(cadena);



        while (ss >> buf)
            tokens.push_back(buf);
        f.close();
        return tokens;


    }

    return tokens;
}

string getAlfabetoArchivo(char* ruta) {

    ifstream f(ruta, ifstream::in);
    char linea[128];

    if (f.fail())
        cerr << "Error al abrir el archivo Pruebas.txt" << endl;
    else {
        f.getline(linea, sizeof (linea));
        f.getline(linea, sizeof (linea));
        f.getline(linea, sizeof (linea));
        string cadena(linea);

        string buf;
        stringstream ss(cadena);

        vector<string> tokens;

        while (ss >> buf)
            tokens.push_back(buf);
        f.close();
        return tokens.at(1);


    }
    return "";

}

vector<tupla*> getTuplasAFDArchivo(vector<tupla*> tuplas, char* ruta) {
    ifstream f(ruta, ifstream::in);
    char linea[128];

    if (f.fail())
        cerr << "Error al abrir el archivo Pruebas.txt" << endl;
    else {
        f.getline(linea, sizeof (linea));
        f.getline(linea, sizeof (linea));
        f.getline(linea, sizeof (linea));

        while (!f.eof()) {
            f.getline(linea, sizeof (linea));
            char * pch;
            string cadena(linea);

            if (cadena.size() == 0)
                continue;

            string edoI;
            string edoF;
            string simbolo;


            string buf;
            stringstream ss(cadena);

            vector<string> tokens;

            while (ss >> buf)
                tokens.push_back(buf);

            edoI = tokens.at(0);
            simbolo = tokens.at(1);
            edoF = tokens.at(2);

            tuplas.push_back(new tupla(edoI, simbolo, edoF));

        }


        f.close();

        return tuplas;
    }
    return tuplas;
}

void getyLlenarMatrizAFN(char* ruta) {
    ifstream f(ruta, ifstream::in);
    int contRengs = 0;
    char linea[128];

    if (f.fail())
        cerr << "Error al abrir el archivo Pruebas.txt" << endl;
    else {
        f.getline(linea, sizeof (linea));
        f.getline(linea, sizeof (linea));
        f.getline(linea, sizeof (linea));

        while (!f.eof()) {
            f.getline(linea, sizeof (linea));

            string cadena(linea);

            string buf;
            stringstream ss(cadena);

            vector<string> tokens;

            while (ss >> buf)
                tokens.push_back(buf);

            for (int i = 0; i < tokens.size(); i++) {
                int asc = atoi(tokens.at(i).c_str());

                if (asc == -1 || asc == -2) {
                    aristas[contRengs][i] = asc;


                } else {
                    std::ostringstream oss;
                    oss << tokens.at(i).at(0);

                    aristas[contRengs][i] = tokens.at(i).at(0);
                }
            }

            contRengs++;

        }
        f.close();

    }
}




