#include <stdio.h>
#include <algorithm>
#include <sys/unistd.h>

#include "AFD.hpp"

AFD::AFD(string edoInicial, vector<string> edosAceptacion) {
    this->edoInicial = edoInicial;
    this->edosAceptacion = edosAceptacion;


}

void AFD::setCadena(string cadena) {
    this->cadena = cadena;
}

void AFD::agregaTupla(tupla* tupla2) {
    tuplas.push_back(tupla2);
}

tupla* AFD::buscatupla(string estadoI, string transicion) {
    for (int i = 0; i < tuplas.size(); i++) {

        if (tuplas[i]->getEstadoI() == estadoI && tuplas[i]->getTrancicion() == transicion)
            return tuplas[i];
    }

    return new tupla("-1", "noValida", "-1");

}

bool AFD::validarCadena() {
    string estadoA = edoInicial;

    int caracter = 0;
    cout << "Cadena a evaluar: " << cadena << endl;
    cout << "tamano de la cadena " << cadena.length() << endl << endl;

    while (caracter < cadena.length()) {
        string objeto = "";
        objeto += cadena[caracter];

        cout << "posicion: :" << caracter << ":  ";
        cout << "edo. Actual= " << estadoA << "  char Actual=" << objeto << endl;
        if (existeTupla(estadoA, objeto)) {

            tupla* tempAp = buscatupla(estadoA, objeto);
            tupla& temp = *tempAp;



            cout << "Tupla a ocupar: ";
            cout << tempAp->getEstadoI() << ", " + tempAp->getTrancicion();
            cout << ", " << tempAp->getEstadoF() << endl << endl;

            estadoA = temp.getEstadoF();

            caracter++;
        } else {
            return false;
        }

    }

    cout << "posicion: :" << caracter << ":  ";
    cout << "edo. Actual= " << estadoA << "  char Actual=" << "-" << endl;

    for (int contEdo = 0; contEdo < edosAceptacion.size(); contEdo++) {

        if (estadoA == edosAceptacion.at(contEdo))
            return true;
    }
    return false;
}

bool AFD::existeTupla(string estadoI, string transicion) {
    for (int i = 0; i < tuplas.size(); i++) {

        if ((tuplas[i]->getEstadoI() == estadoI)&&(tuplas[i]->getTrancicion() == transicion))
            return true;
    }

    return false;

}

void AFD::printTupla(int pos) {
    cout << tuplas[pos]->getEstadoI() << "----" << tuplas[pos]->getTrancicion() << "----" << tuplas[pos]->getEstadoF() << endl;
}

bool comparaTupla(tupla a, tupla b) {
    if ((a.getEstadoI() == b.getEstadoI())&&(a.getEstadoF() == b.getEstadoF())&&(a.getTrancicion() == b.getTrancicion()))
        return true;

    return false;
}

string AFD::generarCadena() {
    string bufferCadena = "";
    string estadoActual = edoInicial;
    vector<tupla*> tuplasTemp;
    int numRand;
    
    tuplasTemp = buscaTuplasEdoI(estadoActual);

    srand(time(NULL));
    numRand = rand() % tuplasTemp.size();
        
    sleep(1);
    
    bufferCadena += tuplasTemp.at(numRand)->getTrancicion();
    estadoActual = tuplasTemp.at(numRand)->getEstadoF();

    
    while (!esEdoFinal(estadoActual)) {
        tuplasTemp.clear();

        tuplasTemp = buscaTuplasEdoI(estadoActual);

        srand(time(NULL));
        numRand = rand() % tuplasTemp.size();
        
        sleep(1);

        bufferCadena += tuplasTemp.at(numRand)->getTrancicion();
        estadoActual = tuplasTemp.at(numRand)->getEstadoF();

    }
    
    return bufferCadena;
}

vector<tupla*> AFD::buscaTuplasEdoI(string estadoI) {
    vector<tupla*> resultado;
    for (int i = 0; i < tuplas.size(); i++) {
        if (estadoI == tuplas.at(i)->getEstadoI())
            resultado.push_back(tuplas.at(i));
    }
    return resultado;
}

bool AFD::esEdoFinal(string edo) {
    for (int i = 0; i < edosAceptacion.size(); i++) {
        if (edo == edosAceptacion.at(i))
            return true;
    }

    return false;
}






