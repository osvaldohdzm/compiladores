#ifndef AFD_HPP
#define	AFD_HPP

#include "tupla.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <stdlib.h>

using namespace std;

class AFD {
public:
    AFD(string edoIncial,vector<string> edosAceptacion);
    void setCadena(string cadena);
    void agregaTupla(tupla* tupla2);
    bool validarCadena();
    string generarCadena();
private:
    vector <tupla*> tuplas;
    vector<string> edosAceptacion;
    string edoInicial;
    string cadena;
    bool existeTupla(string estadoI, string transicion);
    tupla* buscatupla(string estadoI, string transicion);
    bool comparaTupla(tupla a, tupla b);
    void printTupla(int pos);
    vector<tupla*> buscaTuplasEdoI(string estadoI);
    bool esEdoFinal(string edo);

};

#endif	/* AFD_HPP */

