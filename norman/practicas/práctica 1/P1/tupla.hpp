#ifndef TUPLA_HPP
#define	TUPLA_HPP
#include <string>
#include <iostream>
using namespace std;
class tupla {
public:
    tupla(string estadoI, string tracicion, string estadoF); 
    string getEstadoI();
    void setEstadoI(string estadoI);
    string getEstadoF();
    void setEstadoF(string estadoF);
    string getTrancicion();
    void setTrancicion(string trancicion);
    
private:
    string estadoI;
    string estadoF;
    string trancicion;
};

#endif	/* TUPLA_HPP */

