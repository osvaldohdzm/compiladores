#include "AFN.hpp"

int main()  {
    int i, j, X, Y, A, T, F, D;

    // leido en la AFN 
    ifstream fin("AFN.txt");
    fin >> N >> M;
    AFNs = new AFN[N];
    fin >> F;
    for(i=0; i<F; i++)    {
        fin >> X;
        AFN_finalStados.insert(X);
    }
    fin >> T;
    while(T--)   {
        fin >> X >> A >> Y;
        for(i=0; i<Y; i++)  {
            fin >> j;
            AFNs[X].transitions[A][i] = j;
        }
    }
    fin.close();

    // construir el AFD correspondiente
    D = 1;
    AFDstadoss.push_back(new AFDstados);
    AFDstadoss[0]->constituentAFNs[0] = 1;
    epsilonClosure(0, AFDstadoss[0]->constituentAFNs);
    for(j=0; j<N; j++)
        if(AFDstadoss[0]->constituentAFNs[j] == 1 && AFN_finalStados.find(j) != AFN_finalStados.end())  {
            AFDstadoss[0]->finalState = true; AFD_finalstados.push_back(0); break;
        }
    incompleteAFDstadoss.push(0);
    while(!incompleteAFDstadoss.empty()) {
        X = incompleteAFDstadoss.front(); incompleteAFDstadoss.pop();
        for(i=1; i<=M; i++)  {
            NFAmove(AFDstadoss[X]->constituentAFNs, i, AFDstadoss[X]->transitions[i]);
            epsilonClosure(AFDstadoss[X]->transitions[i], AFDstadoss[X]->transitions[i]);
            for(j=0; j<D; j++)
                if(AFDstadoss[X]->transitions[i] == AFDstadoss[j]->constituentAFNs)  {
                   AFDstadoss[X]->symbolicTransitions[i] = j;    break;
                }
            if(j == D)   {
                AFDstadoss[X]->symbolicTransitions[i] = D;
                AFDstadoss.push_back(new AFDstados);
                AFDstadoss[D]->constituentAFNs = AFDstadoss[X]->transitions[i];
                for(j=0; j<N; j++)
                    if(AFDstadoss[D]->constituentAFNs[j] == 1 && AFN_finalStados.find(j) != AFN_finalStados.end())  {
                        AFDstadoss[D]->finalState = true; AFD_finalstados.push_back(D); break;
                    }
                incompleteAFDstadoss.push(D);
                D++;
            }
        }
    }

    // escribir el correspondiente AFD
    ofstream fout("AFD.txt");
    fout << D << " " << M << "\n" << AFD_finalstados.size();
    for(vector<int>::iterator it=AFD_finalstados.begin(); it!=AFD_finalstados.end(); it++)
        fout << " " << *it;
    fout << "\n";
    for(i=0; i<D; i++)  {
        for(j=1; j<=M; j++)
            fout << i << " " << j << " " << AFDstadoss[i]->symbolicTransitions[j] << "\n";
    }
    fout.close();
    cout<<"Programa de conversion de un Automata Finito No determinista(NFA) \n"<<endl;
    cout<<"A un Automata Finito Determista(AFD)"<<endl; 
	cout<<"Se genero un archivo AFD.txt con el automata convertido"<<endl;
    return 0;
}
