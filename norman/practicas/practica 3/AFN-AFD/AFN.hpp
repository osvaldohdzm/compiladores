#ifndef __AFN_HPP__
#define __AFN_HPP__

#include <cstdio>
#include <fstream>
#include <iostream>
#include <bitset>
#include <vector>
#include <cstring>
#include <cstdlib>
#include <algorithm>
#include <queue>
#include <set>
#define MAX_AFN_STADOS 10
#define MAX_SIMBOLOS_SIZE 10
using namespace std;

// Representacion de un estado AFN
class AFN {
    public:
        int transitions[MAX_SIMBOLOS_SIZE][MAX_AFN_STADOS];
        AFN()  {
            for(int i=0; i<MAX_SIMBOLOS_SIZE; i++)
                for(int j=0; j<MAX_AFN_STADOS; j++)
                    transitions[i][j] = -1;
        }
} *AFNs;

// Representacion de un estado AFD
struct AFDstados {
    bool finalState;
    bitset<MAX_AFN_STADOS> constituentAFNs;
    bitset<MAX_AFN_STADOS> transitions[MAX_SIMBOLOS_SIZE];
    int symbolicTransitions[MAX_SIMBOLOS_SIZE];
};

set <int> AFN_finalStados;
vector <int> AFD_finalstados;
vector <AFDstados*> AFDstadoss;
queue <int> incompleteAFDstadoss;
int N, M;   //N - > Numero de stados , M -> Tamano del alfabeto de entrada


// encuentra el cierre �psilon del estado "estado" NFA y la almacena en el "cierre "
void epsilonClosure(int state, bitset<MAX_AFN_STADOS> &closure )    {
    for(int i=0; i<N && AFNs[state].transitions[0][i] != -1; i++)
        if(closure[AFNs[state].transitions[0][i]] == 0)    {
            closure[AFNs[state].transitions[0][i]] = 1;
            epsilonClosure(AFNs[state].transitions[0][i], closure);
        }
}

// encuentra el cierre epsilon de un conjunto de NFA declara "estado" y lo almacena en el "cierre "
void epsilonClosure(bitset<MAX_AFN_STADOS> state, bitset<MAX_AFN_STADOS> &closure) {
    for(int i=0; i<N; i++)
        if(state[i] == 1)
            epsilonClosure(i, closure);
}

// devuelve un bit que representa el conjunto de estados del AFN podria estar en despu�s de mover
// X de estado en s�mbolo de entrada A
void NFAmove(int X, int A, bitset<MAX_AFN_STADOS> &Y)   {
    for(int i=0; i<N && AFNs[X].transitions[A][i] != -1; i++)
        Y[AFNs[X].transitions[A][i]] = 1;
}

// devuelve un bit que representa el conjunto de estados del AFN podr�a estar en despu�s de mover
// desde el conjunto de estados X en el s�mbolo de entrada A
void NFAmove(bitset<MAX_AFN_STADOS> X, int A, bitset<MAX_AFN_STADOS> &Y)   {
    for(int i=0; i<N; i++)
        if(X[i] == 1)
            NFAmove(i, A, Y);
}


#endif
